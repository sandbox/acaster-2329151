<?php

/**
 * @file
 * The default format for addresses.
 */

$plugin = array(
  'title' => t('Coordinates'),
  'format callback' => 'addressfield_coordinates_format_coordinates_generate',
  'type' => 'coordinates',
  'weight' => -99,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_coordinates_format_coordinates_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form') {
    $format['person_coordinates'] = array(
      '#type' => 'textfield',
      '#title' => t('Coordinates'),
      '#description' => t('Enter coordinates "Latitude Longitude". Examples "30.3355655 53.8809935"'),
      '#tag' => 'span',
      '#attributes' => array('class' => array('person-coordinates')),
      '#default_value' => isset($address['person_coordinates']) ? $address['person_coordinates'] : '',
      '#weight' => -200,
    );
  }
  else {
    // Add our own render callback for the format view.
    $format['#pre_render'][] = '_addressfield_coordinates_render_address';
  }
}
