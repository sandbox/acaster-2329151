Extends Address Field[1] by adding a coordinates option (Latitude, Longitude).
The list of options is hardcoded into the module, I would like to make that
configurable in some way.

Based heavily on Garrettc's Address Field Title module[2].

[1] http://drupal.org/project/addressfield
[2] http://drupal.org/project/addressfield_title
